package com.san.vehicle.controller;

import com.san.vehicle.model.RegDtl;
import com.san.vehicle.service.ProcessPdfService;
import com.san.vehicle.service.TpDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sankarvinnakota on 30/11/19.
 */
@RestController
public class VehicleController {

    @Autowired
    private TpDetailsService tpDetailsService;

    @Autowired
    private ProcessPdfService processPdfService;

    @GetMapping("/processFile")
    public void processFile() {
        tpDetailsService.processFile();
    }

    @GetMapping("/readPdf")
    public void readPdf() {
        RegDtl regDtl = new RegDtl("");
        regDtl.setApplicationNo("AP1070084062019");
        processPdfService.readPdf(regDtl);
    }

}
