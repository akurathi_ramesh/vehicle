package com.san.vehicle.service;

import com.san.vehicle.config.PropertyConfig;
import com.san.vehicle.model.RegDtl;
import com.san.vehicle.model.application.ApplicationRequest;
import com.san.vehicle.model.application.ApplicationResponse;
import com.san.vehicle.model.registration.RegistrationResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import static com.san.vehicle.util.VehicleConstants.*;

/**
 * Created by sankarvinnakota on 30/11/19.
 */
@Service
@Slf4j
public class TpDetailsService {

    @Autowired
    private ProcessExcelService processExcelService;

    @Autowired
    private ProcessPdfService processPdfService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PropertyConfig propertyConfig;

    public void processFile() {
        List<String> regNoList = processExcelService.readExcelAndGetRegNos();
        int excelUpdatedCount = 0;
        if (CollectionUtils.isNotEmpty(regNoList)) {
            for(int counter =0; counter<regNoList.size(); counter++) {
                log.info("Processing Record # {} out of {}", (counter+1), regNoList.size());
                RegDtl regDtl = new RegDtl(regNoList.get(counter));
                try {
                    if(setDetailsByRegistrationNo(regDtl)) {
                        log.info("Registration No:{}, Chasis No: {}", regDtl.getRegistrationNo(), regDtl.getChasisNo());
                        if(setApplicationNo(regDtl)) {
                            log.info("Registration No:{}, Chasis No: {}, Application No:{}", regDtl.getRegistrationNo(), regDtl.getChasisNo(), regDtl.getApplicationNo());
                            downloadPdf(regDtl);
                            if(processPdfService.readPdf(regDtl)) {
                                processExcelService.writeExcel(regDtl);
                                excelUpdatedCount++;
                                log.info("Updated excel with details for Registration No:{}, Chasis No: {}, Application No:{}", regDtl.getRegistrationNo(), regDtl.getChasisNo(), regDtl.getApplicationNo());
                                sleep(propertyConfig.getSleepTimeSecs());
                            } else {
                                log.warn("Skipping save excel operation for Registration Number:{}", regDtl.getRegistrationNo());
                            }
                        } else {
                            log.warn("Skipping download pdf for Registration Number:{}", regDtl.getRegistrationNo());
                        }
                    } else {
                        log.warn("Skipping Application No fetch for Registration Number:{}", regDtl.getRegistrationNo());
                    }
                } catch (Exception e) {
                    log.error("Exception while fetching the details for Registration No:{}", regDtl.getRegistrationNo(), e);
                }
            }
        }
        log.info("Processed # records {} and Updated excel for # records:{}", CollectionUtils.size(regNoList), excelUpdatedCount);
    }

    private void sleep(long sleepTimeInSec) {
        try {
            log.info("Waiting for {} secs", sleepTimeInSec);
            Thread.sleep(sleepTimeInSec * 1000);
        } catch (InterruptedException e) {
            log.error("Exception while pausing the thread for {} secs", sleepTimeInSec, e);
        }
    }

    /**
     * 1. https://rtaappsc.epragathi.org:1201/reg/citizenServices/getOtherStateTPDetails?prNo=AP39A0001
     *
     * @param regDtl
     * @return
     */
    public boolean setDetailsByRegistrationNo(RegDtl regDtl) {
        boolean isSuccess = true;
        RegistrationResponse response = restTemplate.getForObject(GET_CHASIS_NO_BY_REG_NO_URL + regDtl.getRegistrationNo(), RegistrationResponse.class);
        // log.info("Registration No: {}, getDetailsByRegistrationNo Response:{}", regDtl.getRegistrationNo(), response);
        regDtl.setChasisNo(response.getResult().getVehicleDetails().getChassisNumber());
        regDtl.setEngineNumber(response.getResult().getVehicleDetails().getEngineNumber());
        if(response.getResult().getInsuranceDetails() != null) {
            regDtl.setPolicyNumber(response.getResult().getInsuranceDetails().getPolicyNumber());
            regDtl.setPolicyValidTill(response.getResult().getInsuranceDetails().getValidTill());
        }
        regDtl.setClassOfVehicle(response.getResult().getClassOfVehicle()); // MCRN for car
        regDtl.setVehicleClass(response.getResult().getClassOfVehicleVO().get(0).getCovdescription());
        regDtl.setPresentAddress(response.getResult().getApplicantDetails().getPresentAddress().getDoorNo());
        if(StringUtils.isBlank(regDtl.getChasisNo())) {
            log.warn("Chasis Number not received for Registration Number:{}, response:{}", regDtl.getRegistrationNo(), response);
            isSuccess = false;
        }
        if(propertyConfig.isFetchCarsOnly() && !VEHICLE_TYPE_CAR.equals(regDtl.getClassOfVehicle())) {
            log.info("Vehicle Type is not Car:{} for Registration Number:{}", regDtl.getClassOfVehicle(), regDtl.getRegistrationNo());
            isSuccess = false;
        }
        log.info("Registration Number:{}, proceed to fetch Application No:{}, regDtl:{}", regDtl.getRegistrationNo(), isSuccess, regDtl);
        return isSuccess;
    }

    /**
     * 2. https://rtaappsc.epragathi.org:1201/reg/citizenServices/applicationSearchForServicesRegisration
     *
     * @param regDtl
     * @return
     */
    public boolean setApplicationNo(RegDtl regDtl) {
        boolean isSuccess = true;
        ApplicationRequest applicationRequest = new ApplicationRequest(regDtl.getRegistrationNo(), regDtl.getChasisNo());
        ApplicationResponse response = restTemplate.postForObject(GET_APPLICATION_NO_URL, applicationRequest, ApplicationResponse.class);
        // log.info("Registration No: {}, Chasis No:{}, getApplicationNo Response:{}", regDtl.getRegistrationNo(), regDtl.getChasisNo(), response);
        regDtl.setApplicationNo(response.getResult().get(0).getApplicationNumber());
        regDtl.setRegisteredOwner(response.getResult().get(0).getApplicantName());
        if(StringUtils.isBlank(regDtl.getApplicationNo())) {
            log.warn("Application Number not received for Registration Number:{}, response:{}", regDtl.getRegistrationNo(), response);
            isSuccess = false;
        }
        log.info("Registration Number:{}, proceed to download pdf:{}, regDtl:{}", regDtl.getRegistrationNo(), isSuccess, regDtl);
        return isSuccess;
    }

    /**
     * 3. https://rtaappsc.epragathi.org:1201/reg/citizenServices/getregcitizenprdetails?applicationNo=
     *
     * @param regDtl
     */
    public void downloadPdf(RegDtl regDtl) {
        String fileName = regDtl.getApplicationNo() + PDF_EXTENSION;
        String pdfPathAndFileName = propertyConfig.getFilePath() + PDF_FOLDER + fileName;
        try {
            FileUtils.copyURLToFile(new URL(DOWNLOAD_PDF_URL + regDtl.getApplicationNo()), new File(pdfPathAndFileName));
            log.info("Saved file:{} for Registration Number:{}", pdfPathAndFileName, regDtl.getRegistrationNo());
        } catch (IOException e) {
            log.error("Exception occurred while downloading the pdf for Registration Number:{}", regDtl.getRegistrationNo(), e);
            throw new RuntimeException("Exception while downloading the pdf", e);
        }
    }

}
