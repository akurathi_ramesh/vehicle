package com.san.vehicle.service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.san.vehicle.config.PropertyConfig;
import com.san.vehicle.model.RegDtl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

import static com.san.vehicle.util.VehicleConstants.*;

/**
 * Created by sankarvinnakota on 30/11/19.
 */
@Service
@Slf4j
public class ProcessPdfService {

    @Autowired
    private PropertyConfig propertyConfig;

    public boolean readPdf(RegDtl regDtl) {
        boolean isSuccess = true;
        String fileName = regDtl.getApplicationNo() + PDF_EXTENSION;
        String pdfPathAndFileName = propertyConfig.getFilePath() + PDF_FOLDER + fileName;

        /*try (PDDocument document = PDDocument.load(new File(pdfPathAndFileName))) {
            if (!document.isEncrypted()) {
                PDFTextStripper tStripper = new PDFTextStripper();
                String pdfFileInText = tStripper.getText(document);
                log.info("pdfFileInText:{}", pdfFileInText);
                String lines[] = pdfFileInText.split("\\r?\\n");
                for (String line : lines) {
                    log.info("{}",line);
                }
            }

        } catch (Exception e) {
            log.error("Exception while reading pdf:{}", pdfPathAndFileName, e);
        }*/
        PdfReader reader;
        try {
            reader = new PdfReader(pdfPathAndFileName);
            String textFromPage = PdfTextExtractor.getTextFromPage(reader, 1);
            //log.info("{}", textFromPage);
            String lines[] = textFromPage.split("\\r?\\n");
            //log.info("Printing Each line from total lines:{}",lines.length);
            boolean containsHypotecatedTo = false;
            boolean containsRegisteringAuthority = false;

            for (String line : lines) {
                //log.info("{}",line);
                if(StringUtils.contains(line, MOBILE_NO)) {
                    regDtl.setMobileNo(StringUtils.substringAfter(line, MOBILE_NO));
                }
                if(containsHypotecatedTo) {
                    regDtl.setHypothecatedTo(StringUtils.substringAfter(line, COLON_SPACE));
                }
                containsHypotecatedTo = StringUtils.contains(line, HYPOTHECATED_TO);
                if(StringUtils.contains(line, MAKERS_NAME)) {
                    regDtl.setMakersName(StringUtils.substringBetween(line, MAKERS_NAME, ENGINE_POWER));
                }
                if(containsRegisteringAuthority) {
                    regDtl.setRegisteringAuthority(line);
                }
                containsRegisteringAuthority = StringUtils.contains(line, REGISTERTING_AUTORITY);
            }
            reader.close();
        } catch (IOException e) {
            log.error("Exception while reading pdf:{}", pdfPathAndFileName, e);
            isSuccess = false;
        }
        log.info("Registration Number:{}, proceed to save excel:{}, regDtl:{}", regDtl.getRegistrationNo(), isSuccess, regDtl);
        return isSuccess;
    }
}
