
package com.san.vehicle.model.application;

import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "httpStatus",
    "message",
    "result"
})
@Data
public class ApplicationResponse {

    @JsonProperty("status")
    private Boolean status;
    @JsonProperty("httpStatus")
    private String httpStatus;
    @JsonProperty("message")
    private String message;
    @JsonProperty("result")
    private List<Result> result = null;
    @JsonProperty("additionalProperties")
    private Map<String, Object> additionalProperties;

}
