
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mandalCode",
    "mandalName",
    "districtId",
    "nonTransportOffice",
    "transportOfice",
    "hsrpoffice",
    "status",
    "name"
})
public class Mandal {

    @JsonProperty("mandalCode")
    private Integer mandalCode;
    @JsonProperty("mandalName")
    private String mandalName;
    @JsonProperty("districtId")
    private Integer districtId;
    @JsonProperty("nonTransportOffice")
    private String nonTransportOffice;
    @JsonProperty("transportOfice")
    private String transportOfice;
    @JsonProperty("hsrpoffice")
    private String hsrpoffice;
    @JsonProperty("status")
    private String status;
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("mandalCode")
    public Integer getMandalCode() {
        return mandalCode;
    }

    @JsonProperty("mandalCode")
    public void setMandalCode(Integer mandalCode) {
        this.mandalCode = mandalCode;
    }

    @JsonProperty("mandalName")
    public String getMandalName() {
        return mandalName;
    }

    @JsonProperty("mandalName")
    public void setMandalName(String mandalName) {
        this.mandalName = mandalName;
    }

    @JsonProperty("districtId")
    public Integer getDistrictId() {
        return districtId;
    }

    @JsonProperty("districtId")
    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    @JsonProperty("nonTransportOffice")
    public String getNonTransportOffice() {
        return nonTransportOffice;
    }

    @JsonProperty("nonTransportOffice")
    public void setNonTransportOffice(String nonTransportOffice) {
        this.nonTransportOffice = nonTransportOffice;
    }

    @JsonProperty("transportOfice")
    public String getTransportOfice() {
        return transportOfice;
    }

    @JsonProperty("transportOfice")
    public void setTransportOfice(String transportOfice) {
        this.transportOfice = transportOfice;
    }

    @JsonProperty("hsrpoffice")
    public String getHsrpoffice() {
        return hsrpoffice;
    }

    @JsonProperty("hsrpoffice")
    public void setHsrpoffice(String hsrpoffice) {
        this.hsrpoffice = hsrpoffice;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
