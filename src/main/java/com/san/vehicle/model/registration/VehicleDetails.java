
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "chassisNumber",
    "engineNumber",
    "bodyTypeDesc",
    "seatingCapacity",
    "rlw",
    "ulw",
    "gvw"
})
public class VehicleDetails {

    @JsonProperty("chassisNumber")
    private String chassisNumber;
    @JsonProperty("engineNumber")
    private String engineNumber;
    @JsonProperty("bodyTypeDesc")
    private String bodyTypeDesc;
    @JsonProperty("seatingCapacity")
    private String seatingCapacity;
    @JsonProperty("rlw")
    private Integer rlw;
    @JsonProperty("ulw")
    private Integer ulw;
    @JsonProperty("gvw")
    private Integer gvw;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("chassisNumber")
    public String getChassisNumber() {
        return chassisNumber;
    }

    @JsonProperty("chassisNumber")
    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    @JsonProperty("engineNumber")
    public String getEngineNumber() {
        return engineNumber;
    }

    @JsonProperty("engineNumber")
    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    @JsonProperty("bodyTypeDesc")
    public String getBodyTypeDesc() {
        return bodyTypeDesc;
    }

    @JsonProperty("bodyTypeDesc")
    public void setBodyTypeDesc(String bodyTypeDesc) {
        this.bodyTypeDesc = bodyTypeDesc;
    }

    @JsonProperty("seatingCapacity")
    public String getSeatingCapacity() {
        return seatingCapacity;
    }

    @JsonProperty("seatingCapacity")
    public void setSeatingCapacity(String seatingCapacity) {
        this.seatingCapacity = seatingCapacity;
    }

    @JsonProperty("rlw")
    public Integer getRlw() {
        return rlw;
    }

    @JsonProperty("rlw")
    public void setRlw(Integer rlw) {
        this.rlw = rlw;
    }

    @JsonProperty("ulw")
    public Integer getUlw() {
        return ulw;
    }

    @JsonProperty("ulw")
    public void setUlw(Integer ulw) {
        this.ulw = ulw;
    }

    @JsonProperty("gvw")
    public Integer getGvw() {
        return gvw;
    }

    @JsonProperty("gvw")
    public void setGvw(Integer gvw) {
        this.gvw = gvw;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
