
package com.san.vehicle.model.registration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "doorNo",
    "mandal",
    "district",
    "state"
})
public class PresentAddress {

    @JsonProperty("doorNo")
    private String doorNo;
    @JsonProperty("mandal")
    private Mandal mandal;
    @JsonProperty("district")
    private District district;
    @JsonProperty("state")
    private State state;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("doorNo")
    public String getDoorNo() {
        return doorNo;
    }

    @JsonProperty("doorNo")
    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    @JsonProperty("mandal")
    public Mandal getMandal() {
        return mandal;
    }

    @JsonProperty("mandal")
    public void setMandal(Mandal mandal) {
        this.mandal = mandal;
    }

    @JsonProperty("district")
    public District getDistrict() {
        return district;
    }

    @JsonProperty("district")
    public void setDistrict(District district) {
        this.district = district;
    }

    @JsonProperty("state")
    public State getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(State state) {
        this.state = state;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
